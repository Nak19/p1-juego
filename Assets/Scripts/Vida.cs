﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida : MonoBehaviour
{
    public float valor = 100;
    public Vida Ref1;
    //public float multiplicadorDaño = 1.0f;
    //public GameObject textoFlotantePrefab;
    public float dañoTotal;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RecibirDaño(float daño)
    {
        //daño *= multiplicadorDaño;
        if (Ref1 != null)
        {
            Ref1.RecibirDaño(daño);
            return;
        }

        valor -= daño;
        dañoTotal = daño;
        //if (valor >= 0) MostrarTextoFloat();
        //if (valor < 0)
        //{
        //    valor = 0;
        //    MostrarTextoFloat();
        //}
    }

    //void MostrarTextoFloat()
    //{
    //    var go = Instantiate(textoFlotantePrefab, transform.position, Quaternion.identity, transform);
    //    go.GetComponent<TextMesh>().text = dañoTotal.ToString();
    //}
}
