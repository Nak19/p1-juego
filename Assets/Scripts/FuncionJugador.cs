﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FuncionJugador : MonoBehaviour
{
    public Vida vida;
    public bool Vida0 = false;
    [SerializeField] private Animator animadorPerder;
    //public Puntaje puntaje;

    void Start()
    {
        vida = GetComponent<Vida>();
        //puntaje.valor = 0;
        AudioListener.volume = 0.5f;
    }

    void Update()
    {
        RevisarVida();
    }

    public void startGame()
    {
        //timer.startTimer();
    }

    public void endGame()
    {
        //timer.startTimer();
        //timer.stopTimer();
    }

    void RevisarVida()
    {
        if (Vida0) return;
        if (vida.valor <=0)
        {
            AudioListener.volume = 0f;
            animadorPerder.SetTrigger("Mostrar");
            Vida0 = true;
            SceneManager.LoadScene(0);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Enemigo")
        {
            gameObject.GetComponent<AudioSource>().volume = 0f;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemigo")
        {
            gameObject.GetComponent<AudioSource>().volume = 0f;
        }
    }
}
