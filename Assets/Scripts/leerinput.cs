﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class leerinput : MonoBehaviour
{
    public InputField inputField;

    public void getData()
    {
        string cadena = "";

        cadena = inputField.text;
        Debug.Log(cadena);

        PlayerPrefs.SetString("nombre", cadena);

        leerData();
    }

    void leerData()
    {
        Debug.Log("El nombre del jugador es:" + PlayerPrefs.GetString("nombre"));
    }
}
