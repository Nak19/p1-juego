﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private int minutos;

    [SerializeField]
    private int segundos;

    [SerializeField]
    private Text timer;

    private int m, s;

   

    private GameControl gameControl;

    public GameObject canvasGameOver;

    // Start is called before the first frame update
    void Start()
    {
        m = minutos;
        s = segundos;
        writeTimer(m, s);
        Invoke("updateTimer", 1f);
        gameControl = gameObject.GetComponent<GameControl>();
        
    }

    public void startTimer()
    {
        CancelInvoke();
    }

    public void stopTimer()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void updateTimer()
    {
        s--;
        if(s < 0)
        {
            if(m==0)
            {
                canvasGameOver.SetActive(true);
                Time.timeScale = 0f;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                AudioListener.pause = true;

            }
            else
            {
                m--;
                s = 59;
            }
        }
        writeTimer(m, s);
        Invoke("updateTimer", 1f);
    }

    private void writeTimer(int m, int s)
    {
        if(s < 10)
        {
            timer.text = m.ToString() + ":0" + s.ToString();
        }
        else
        {
            timer.text = m.ToString() + ":" + s.ToString();
        }
    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
