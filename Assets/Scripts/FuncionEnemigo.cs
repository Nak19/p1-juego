﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FuncionEnemigo : MonoBehaviour
{
    private GameObject target;
    private NavMeshAgent agente;
    private Vida vida;
    private Animator animator;
    private Collider collider;
    private Vida vidaJugador;
    private FuncionJugador funcionJugador;
    public bool Vida0 = false;
    public bool estaAtacando = false;
    public float velocidad = 1.0f;
    public float angularSpeed = 120;
    public float daño = 35; // daño del enemigo
    public bool encontrado;
    //public bool sumarPuntos = false;
    //public GameObject puntajePantalla;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player");
        vidaJugador = target.GetComponent<Vida>();
        if (vidaJugador == null)
        {
            throw new System.Exception("El Jugador no tiene vida");
        }

        funcionJugador = target.GetComponent<FuncionJugador>();

        if (funcionJugador == null)
        {
            throw new System.Exception("El Jugador no tiene componente FuncionJugador");
        }

        agente = GetComponent<NavMeshAgent>();
        vida = GetComponent<Vida>();
        animator = GetComponent<Animator>();
        collider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        //RevisarVida();
        Perseguir();
        RevisarAtaque();
        JugadorEncontrado();

    }

    void JugadorEncontrado()
    {
        Vector3 adelante = transform.forward;
        Vector3 targetJugador = (GameObject.Find("Player").transform.position - transform.position).normalized;

        // ángulo de vision del enemigo para atacar
        if (Vector3.Dot(adelante, targetJugador) < 0.6f)
        {
            encontrado = false;
        }
        else
        {
            encontrado = true;
        }
    }

    //void RevisarVida()
    //{
    //    //if (Vida0) return;
    //    if (vida.valor <= 0)
    //    {
    //        //sumarPuntos = true;
    //        //if(sumarPuntos)
    //        //{
    //        //    puntajePantalla.GetComponent<Puntaje>().valor += 10;
    //        //    sumarPuntos = false;
    //        //}
    //        Vida0 = true;
    //        agente.isStopped = true;
    //        collider.enabled = false;
    //        animator.CrossFadeInFixedTime("Vida0", 0.1f);
    //        Destroy(gameObject, 3f);
    //    }
    //}

    void Perseguir()
    {
        if (Vida0) return;
        if (funcionJugador.Vida0) return;
        agente.destination = target.transform.position;
    }

    void RevisarAtaque()
    {
        if (Vida0) return;
        if (estaAtacando) return;
        if (funcionJugador.Vida0) return;
        float distanciaAggro = Vector3.Distance(target.transform.position, transform.position);

        if (distanciaAggro <= 2.0 && encontrado)
        {
            Atacar();
        }
    }

    void Atacar()
    {
        vidaJugador.RecibirDaño(daño);
        agente.speed = 0;
        agente.angularSpeed = 0;
        estaAtacando = true;
        animator.SetTrigger("DebeAtacar");
        Invoke("ReiniciarAtaque", 1.5f);
    }

    void ReiniciarAtaque()
    {
        estaAtacando = false;
        agente.speed = velocidad;
        agente.angularSpeed = angularSpeed;
    }
}
